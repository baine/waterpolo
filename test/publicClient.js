
var assert = require('chai').assert;

describe('PublicClient', function() {
    const waterpolo = require('../index.js');
    const publicClient = new waterpolo.PublicClient();
    it('should get ticker', function(done) {
        publicClient.returnTicker({}, function(err, {statusCode, body}) {
            if (err) {
                done(err);
            } else {
                assert.isObject(body);
                assert.equal(statusCode, 200);
                done();
            }
        });
    });
});
