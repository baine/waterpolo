
var Promise = require('bluebird');

var fs = Promise.promisifyAll(require('fs'));
var waterpolo = require('../index.js');
var assert = require('chai').assert;

function getClient() {
    return fs.readFileAsync(`${process.env.HOME}/.waterpolo`, 'utf8')
        .then(function(x) {
            let credentials = {};

            let lines = x.split('\n');
            for (let line of lines) {
                let i = line.indexOf('=');
                if (i != -1) {
                    credentials[line.substring(0, i)] = line.substring(i + 1);
                }
            }
            const client = new waterpolo.TradingClient(credentials);
            return Promise.promisifyAll(client);
        });
}

describe('TradingClient', function() {
    describe('basic functionality of returnActiveLoans', function() {
        it('should return', function(done) {
            getClient()
                .then(function(tradingClient) {
                    return tradingClient.returnActiveLoansAsync({});
                })
                .then(function(result) {
                    assert(result.body.provided.length > 1000);
                })
                .then(done)
                .catch(done);
        });

        describe('invalid credentials', function() {
            it('should return a 403', function(done) {
                let c = Promise.promisifyAll(new waterpolo.TradingClient({ key: 'k', secret: 'abcd' }));
                c.returnActiveLoansAsync({})
                    .then(function(response) {
                        assert.equal(response.body.error, 'Invalid API key/secret pair.');
                        assert.equal(response.statusCode, 403);
                    })
                    .then(done)
                    .catch(done);
            });
        });
    });
});
