
const crypto = require('crypto');
const request = require('request');

function isInvalidNonce(response) {
    return response.statusCode == 422
        && response.body
        && /nonce/i.test(response.body.error);
}

/**
 * {
 *   statusCode: 429,
 *   body: {
 *     error: 'Too many requests. Please try again in a moment.'
 *   }
 * }
 *
 */
function isTooManyRequests(response) {
    return response.statusCode == 429
        && response.body
        && /Too many requests/i.test(response.body.error);
}

/**
 * {
 *     statusCode: 200,
 *     body: {
 *         error: 'Connection timed out. Please try again.'
 *     }
 * }
 */
function isConnectionTimedout(response) {
    return response.statusCode == 200
        && response.body
        && /Connection timed out/i.test(response.body.error);
}

function retry(f, i, n, cb) {
    f(function(err, response) {
        if (err) {
            console.log('we currently do not consider errors retryable; error was:');
            console.log(err);
            cb(err, response);
        } else if (i === n) {
            console.log('number of retries exhausted');
            cb(err, response);
        } else if (response &&
                   (isInvalidNonce(response)
                    || isTooManyRequests(response)
                    || isConnectionTimedout(response))) {
            let random = 0.9 + 0.1 * Math.random();
            const delay = 10000 * random * Math.pow(2, i);
            console.log(`delaying for ${delay}ms`);
            setTimeout(retry, delay, f, i + 1, n, cb);
        } else {
            cb(err, response);
        }
    });
}

function r(defaultOptions, options, callback) {
    let combinedOptions = Object.assign({}, defaultOptions, options);
    request(combinedOptions, function(err, response, body) {
        if (err) {
            return callback(err, body);
        } else {
            let parsedJson = null;
            try {
                parsedJson = JSON.parse(body);
            } catch(e) {
                console.log('status code was', response.statusCode);
                console.log('could not parse json', e);
                console.log('body was ', body);
                return callback(e, null);
            }
            return callback(null, {
                statusCode: response && response.statusCode,
                body: parsedJson
            });
        }
    });
}

function get(options, command, query, callback) {
    let f = function(cb) {
        r(options, {
            method: 'GET',
            url: 'https://poloniex.com/public',
            qs: Object.assign({}, query, { command: command})
        }, cb);
    };
    retry(f, 0, 5, callback);
}

function post(credentials, options, command, query, callback) {
    let f = function(cb) {
        const qs = Object.assign({},
                                 query,
                                 { nonce: 100 * new Date().getTime(), command: command });
        const signedBody = Object.keys(qs)
              .map(k => `${encodeURIComponent(k)}=${encodeURIComponent(qs[k])}`)
              .join('&');
        const signature = crypto
              .createHmac('sha512', credentials.secret)
              .update(signedBody)
              .digest('hex');
        r(options, {
            method: 'POST',
            url: 'https://poloniex.com/tradingApi',
            body: signedBody,
            headers: {
                Key: credentials.key,
                Sign: signature,
                'content-type': 'application/x-www-form-urlencoded'
            }
        }, cb);
    };
    retry(f, 0, 5, callback);
}

function T({ key, secret }, options={ timeout:20000 }) {
    this.credentials = {key, secret};
    this.options = options;
}

function P(options={ timeout:20000 }) {
    this.options = options;
}

[
    'returnBalances',
    'returnCompleteBalances',
    'returnDepositAddresses',
    'generateNewAddress',
    'returnDepositsWithdrawals',
    'returnOpenOrders',
    'returnTradeHistory',
    'returnOrderTrades',
    'buy',
    'sell',
    'cancelOrder',
    'moveOrder',
    'withdraw',
    'returnFeeInfo',
    'returnAvailableAccountBalances',
    'returnTradableBalances',
    'transferBalance',
    'returnMarginAccountSummary',
    'marginBuy',
    'marginSell',
    'getMarginPosition',
    'closeMarginPosition',
    'createLoanOffer',
    'cancelLoanOffer',
    'returnOpenLoanOffers',
    'returnActiveLoans',
    'returnLendingHistory',
    'toggleAutoRenew'
].forEach(command => {
    T.prototype[command] = function(params, cb) {
        post(this.credentials, this.options, command, params, cb);
    };
});

[
    'returnTicker',
    'return24hVolume',
    'returnOrderBook',
    'returnTradeHistory',
    'returnChartData',
    'returnCurrencies',
    'returnLoanOrders'
].forEach(command => {
    P.prototype[command] = function(params, cb) {
        get(this.options, command, params, cb);
    };
});

module.exports = {
    TradingClient: T,
    PublicClient: P
};
